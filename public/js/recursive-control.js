function recursively_ajax_one() {
    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/clock',
        dataType: 'json',
        success: function(data) {
            //console.warn(data.DateTime);
            document.getElementById('time-date').innerHTML = data.DateTime;

            setTimeout(recursively_ajax_one, 1000);
        }
    });
}
recursively_ajax_one();


/*
function recursively_ajax_two() {

    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/timmer_actions',
        dataType: 'json',
        success: function(data) {
            console.warn("get jvm info success");
            document.getElementById('action').innerHTML = data.timmerAction;
            document.getElementById('timmer-number').innerHTML = data.timeNow;

            setTimeout(recursively_ajax_two, 1000);
        }
    });
}
recursively_ajax_two();*/



function recursively_configuration() {

    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/config',
        dataType: 'json',
        success: function(config) {
            //console.warn("get jvm info success");
            //document.getElementById('action').innerHTML = data.configuration;
            
            timeSplit = config.time.split(":");
            if (parseInt(timeSplit[0]) < 10){ timeSplit[0] = "0"+timeSplit[0]};
            if (parseInt(timeSplit[1]) < 10){ timeSplit[1] = "0"+timeSplit[1]};
            console.log(timeSplit);
            document.getElementById('timmer-number').innerHTML = timeSplit[0]+":"+timeSplit[1];

            document.getElementById('team-name-1').innerHTML = config.teamOneName;
            document.getElementById('team-score-1').innerHTML = config.teamOneScore;
          
            document.getElementById('team-name-2').innerHTML = config.teamTwoName;
            document.getElementById('team-score-2').innerHTML = config.teamTwoScore;
          
            document.getElementById('fouls-score-1').innerHTML = config.teamOneFouls;
            document.getElementById('fouls-score-2').innerHTML = config.teamTwoFouls;
            document.getElementById('period-number').innerHTML = config.period;

            console.log(config.position);

            if (config.position === "1"){
                document.getElementById("position-1").style.visibility = "visible";
                document.getElementById("position-2").style.visibility = "hidden";


            }


            if (config.position === "2"){
                document.getElementById("position-2").style.visibility = "visible";
                document.getElementById("position-1").style.visibility = "hidden";
            }



            setTimeout(recursively_configuration, 5000);
        }
    });
}
recursively_configuration();