function recursively_ajax_one() {
    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/clock',
        dataType: 'json',
        success: function(data) {
            //console.warn(data.DateTime);
            //$("#time-date").html(data.DateTime);
            document.getElementById("time-date").innerHTML =data.DateTime;

            setTimeout(recursively_ajax_one, 1000);
        }
    });
}
recursively_ajax_one();


/*
function recursively_ajax_two() {

    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/timmer_actions',
        dataType: 'json',
        success: function(data) {
            console.warn("get jvm info success");
            $("#action").html( data.timmerAction;
            $("#timmer-number").html( data.timeNow;

            setTimeout(recursively_ajax_two, 1000);
        }
    });
}
recursively_ajax_two();*/



function recursively_configuration() {

    console.warn("begin");
    $.ajax({
        type: "GET",
        url: '/config',
        dataType: 'json',
        success: function(config) {
            //console.warn("get jvm info success");
            //$("#action").html( data.configuration;
            
            timeSplit = config.time.split(":");
            if (parseInt(timeSplit[0]) < 10){ timeSplit[0] = "0"+timeSplit[0]};
            if (parseInt(timeSplit[1]) < 10){ timeSplit[1] = "0"+timeSplit[1]};
            console.log(timeSplit);

         //   $("#timmer-number").html(timeSplit[0]+":"+timeSplit[1]);
            document.getElementById("timmer-number").innerHTML =timeSplit[0]+":"+timeSplit[1] ;


            $("#team-name-1").html(config.teamOneName);
            $("#team-score-1").html(config.teamOneScore);
          
            $("#team-name-2").html(config.teamTwoName);
            $("#team-score-2").html(config.teamTwoScore);
          
            $("#fouls-score-1").html(config.teamOneFouls);
            $("#fouls-score-2").html(config.teamTwoFouls);
            $("#period-number").html(config.period);

            console.log(config.position);

            if (config.position === "1"){
                $("#position-1").css({"visibility": "visible"});
                $("#position-2").css({"visibility": "hidden"});


            }


            if (config.position === "2"){
                $("#position-2").css({"visibility": "visible"});
                $("#position-1").css({"visibility": "hidden"});
            }



            setTimeout(recursively_configuration, 500);
        }
    });
}
recursively_configuration();