function gitPull(dbName) {
    console.log("git pull", dbName);

    var bip = new Audio('audio/bip-4.wav');
    bip.play();
    $.ajax({
        type: "GET",
        url: "/gitpull",
        data: { "database": dbName },
        success: (function (data) {
            console.log(data);
            alert(data);

        })

    });
}


function gitBranchA(dbName) {
    console.log("git branch", dbName);

    var bip = new Audio('audio/bip-4.wav');
    bip.play();
    $.ajax({
        type: "GET",
        url: "/gitbranch",
        data: { "database": dbName },
        success: (function (data) {
            console.log(data);

            var myArray = data.split("\n");
            console.log(myArray);

            var branchs = '<table style="width: 40%;">';
            for (let value of myArray) {

                if (value !== "") {
                    branchs = branchs + ` 
            <tr>
            <td> <p>${value} <p></td>         
            <td>  <a onclick="checkout('${value}')" class="btn btn-warning">Checkout</a> </td>
            <td>  <a onclick="upload('${value}')" class="btn btn-danger">Upload</a> </td>
            <td>  <a onclick="upgrade('${value}')" class="btn btn-dark">Upgrade</a> </td>
            </tr>` }

            };

            branchs = branchs + '</table>';

            $(".cont-replace").empty().append(branchs);


        })

    });
}






function gitBranchATwo(dbName) {
    console.log("git branch", dbName);

    var bip = new Audio('audio/bip-4.wav');
    bip.play();
    $.ajax({
        type: "GET",
        url: "/gitbranch",
        data: { "database": dbName },
        success: (function (data) {
            console.log(data);

            var myArray = data.split("\n");
            console.log(myArray);

            var branchs = '<div>';
            for (let value of myArray) {

                if (value !== "") {
                    branch = value.replaceAll("remotes/origin/", "").replaceAll(" ", "")
                    branchName = branch.replaceAll("*", ">>_");
                    if ((branchName.search(">>_")) > -1){
                        branchName = '<p style="color:red">'+branchName+ '</p>';
                    }
                    
                    branch = branch.replaceAll("*", "").replaceAll("* ", "");


                    console.log(branch);
                    branchs = branchs + ` 
                    <div style="word-wrap: break-word;"> <p>${branchName} <p></div>  
                        <div class="d-flex justify-content-between">  
                            <button onclick="gitCheckout('${dbName}', '${branch}')" class="btn btn-warning">Checkout</button>
                            <!--button onclick="upload('${value}')" class="btn btn-danger">Upload</button-->
                            <!--button onclick="upgrade('${value}')" class="btn btn-dark">Upgrade</button--> 
                    </div>
            
            ` }

            };

            branchs = branchs + '</div>';


            $(`.cont-replace-${dbName}`).empty().append(branchs);


        })

    });

}





function gitCheckout(dbName, branch) {
    console.log("git pull", dbName);

    var bip = new Audio('audio/bip-4.wav');
    bip.play();
    $.ajax({
        type: "GET",
        url: "/gitcheckout",
        data: { "database": dbName, "branch": branch },
        success: (function (data) {
            console.log(data);
            alert(data);

        })

    });

    gitBranchATwo(dbName);
}








function server(dbName, order) {
    console.log("order", order);

    var bip = new Audio('audio/bip-4.wav');
    bip.play();
    $.ajax({
        type: "GET",
        url: "/server",
        data: { "database": dbName, "order": order },
        success: (function (data) {
            console.log(data);
            alert(data);

        })

    });
}




function uploadCode(dbName){server(dbName, "upload") }
function restartService(dbName){server(dbName, "restart") }
function stopService(dbName){server(dbName, "stop") }