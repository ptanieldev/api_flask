function showTime() {
    setInterval(function() {

        var today = new Date();
        var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        //var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        let hours = today.getHours();
        let mins = today.getMinutes();
        let segs = today.getSeconds();

        if (hours < 10) { hours = "0" + hours; }
        if (mins < 10) { mins = "0" + mins; }
        if (segs < 10) { segs = "0" + segs; }
        time = hours + ":" + mins + ":" + segs;

        var dateTime = date + ' ' + time;

        try {
            document.getElementById('date-time').innerHTML = dateTime;
        } catch {}

    }, 100);
}

showTime();