
from database.query_orm.user import UserDB
from flask import  jsonify
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import jwt
from const.application import SECRET_TOKEN

class token:
    def generate(auth):
        user = UserDB.get(auth["username"])
        if not user:
            return {'error': 'usuario no existe'}
        
        if check_password_hash(user["password"], auth["password"]): 
            token = jwt.encode({'public.id': user.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, SECRET_TOKEN)  
            return {"token": token}
        return {"error": "invalid_password"}



 

