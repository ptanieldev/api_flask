from flask import Flask, request, make_response, redirect
from functools import wraps
from const.application import SECRET_TOKEN
from database.models.user import User
import jwt

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        #if 'x-access-tokens' in request.headers:
        #    token = request.headers['x-access-tokens']
        #    print (token)
        if 'x-access-tokens' in request.cookies:
             token = request.cookies['x-access-tokens']
             print (token)
        
        if not token:
            print ("a valid token is missing")
            return make_response(redirect('/login'))
            #return {'error': 'a valid token is missing'}

        try:
            data = jwt.decode(token, SECRET_TOKEN, algorithms=['HS256'])
            current_user = User.objects(username=data["public.id"]).first()
        except:
            print({'error': 'token is invalid'})
            return make_response(redirect('/login'))
            #return {'error': 'token is invalid'}

        return f(current_user, *args, **kwargs)
    return decorator

# TODO: def api_token
