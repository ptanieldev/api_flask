from flask import Blueprint

from controllers.session_controller import login_user
session_routes = Blueprint("session_routes", __name__, template_folder="../views/session")


"""@session_routes.route("/hello", methods=['GET'])
def hello():  
    return "<h1>Hello world</h1>"
"""


@session_routes.route('/login', methods=['GET', 'POST'])  
def login():
    return login_user()