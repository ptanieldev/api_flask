from flask import Blueprint

from controllers.user_controller import create_user
from decorators.auth import token_required
user_routes = Blueprint("user_routes", __name__, template_folder="views")


@user_routes.route("/user", methods=['GET'])
@token_required
def user(token):  
    return "<h1>Hello user</h1>"


@user_routes.route('/user', methods=['POST'])
@token_required
def create():
    return create_user()