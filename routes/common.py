from flask import Blueprint, render_template, request
from const.c7 import C7_COMPANIES, IDCA_SYSTEMS
common_routes = Blueprint("common_routes", __name__, template_folder="../views")
from decorators.auth import token_required
from controllers.common_controller import create_backup, git_branch_a, git_pull_a, git_checkout_a, server_control


@common_routes.route("/", methods=['GET'])
@token_required
def index(token):
    print(token)
    return render_template("index.html", companies=C7_COMPANIES, idca_systems =IDCA_SYSTEMS)



@common_routes.route('/backup', methods=['POST'])
@token_required
def backup(token):
    return create_backup(request.json)

@common_routes.route('/gitpull', methods=['GET'])
@token_required
def git_pull(token):
    return git_pull_a(request.args.get("database"))

@common_routes.route('/gitbranch', methods=['GET'])
@token_required
def git_branch(token):
    return git_branch_a(request.args.get("database"))

@common_routes.route('/gitcheckout', methods=['GET'])
@token_required
def git_checkout(token):
    return git_checkout_a(request.args.get("database"), request.args.get("branch"))

@common_routes.route('/server', methods=['GET'])
@token_required
def server(token):
    return server_control(request.args.get("database"), request.args.get("order"))