

from const.c7 import C7_COMPANIES
from utils.ssh import Backups

def git_pull_a(database):
    for company in C7_COMPANIES:
        if (database == company["name"]):
            return Backups.git_pull(company)
    return ("no encontrado")

def git_branch_a(database):
    for company in C7_COMPANIES:
        if (database == company["name"]):
            return Backups.git_branch(company)
    return ("no encontrado")

def create_backup(data):
    for company in C7_COMPANIES:
        if (data["database"] == company["name"]):
            print  (company)
            return Backups.create_backup(company)
    return ("no encontrado")


def git_checkout_a(database, branch):
    for company in C7_COMPANIES:
        if (database == company["name"]):
            return Backups.git_checkout(company, branch)
    return ("no encontrado")

def server_control(database, order):
    for company in C7_COMPANIES:
        command= ""
        if ((database == company["name"]) and (order == "upload")):
            command = "docker cp {0}/src/modules/. c7-odoo:/opt/odoo/odoo/addons".format(company["folder"])
        if ((database == company["name"]) and (order == "restart")):
            command = "docker exec --tty  c7-odoo service odoo restart".format(company["folder"])
        if ((database == company["name"]) and (order == "stop")):
            command = "docker exec --tty  c7-odoo service odoo stop".format(company["folder"])

        if (command !=""):
            print(command)
            return Backups.ssh_exec_cmd(company["server"], company["username"], company["password"], command)
    return ("no encontrado")
