
from email import message
from flask import render_template, send_from_directory, request, make_response, redirect
from auth.passport import token
from database.query_orm.user import UserDB




def login_user():
    if request.method == 'GET':
        return render_template("login.html")

    auth = token.generate(request.form)
    if "error" in auth: 
        return render_template("login.html", message =auth)
    if "token" in auth:
        resp = make_response(redirect('/'))
        resp.set_cookie('x-access-tokens', auth["token"])  
        return resp
              
        
