from flask import Flask, send_from_directory
from flask_mongoengine import MongoEngine
from const.application import SECRET_TOKEN, APP_PORT
from const.database import DB_CONFIG
from routes.session import session_routes
from routes.user import  user_routes
from routes.common import common_routes


app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = DB_CONFIG
app.secret_key = SECRET_TOKEN
db = MongoEngine()
db.init_app(app)

app.register_blueprint(session_routes, url_prefix="")
app.register_blueprint(user_routes, url_prefix="")
app.register_blueprint(common_routes, url_prefix="")


#public folder
@app.route("/<path:path>")
def static_dir(path):
    return send_from_directory("public", path)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=APP_PORT, debug=True)