from database.models.user import User
from flask import Flask, render_template, jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import jwt

class UserDB:
    @classmethod
    def save(cls,  username, email, password):
        hashed_password = generate_password_hash(password, method='sha256')
        user = User(username=username,
                    email=email,
                    password=hashed_password)
        user.save()
        return jsonify(user.to_json())

    @classmethod 
    def get(cls, username):
        if username:
            return User.objects(username=username).first()





    