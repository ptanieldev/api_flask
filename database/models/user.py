
from flask_mongoengine import MongoEngine
db = MongoEngine()


class User(db.Document):
    username = db.StringField(required=True, unique=True)
    email = db.StringField(required=True)
    password = db.StringField(required=True)

    def to_json(self):
        return {
            "action": "success created",
            "data": {
                "username": self.username,
                "email": self.email,
                "password": self.password}
        }
