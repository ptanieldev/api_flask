import paramiko
from datetime import datetime


class Backups:
    @classmethod
    def ssh_exec_cmd(cls, url: str, user: str, passwd: str, command: str,prt: int =22) -> str:
        conn = paramiko.SSHClient()
        conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())  
        conn.connect(url, port=prt, username=user, password=passwd)
        #print("executing command please wait")
        stdin, stdout, stderr = conn.exec_command(command)
        response = stdout.readlines()
        response = "".join(response)
        print(response)
        return response

    @classmethod
    def create_backup(cls, data)->str:
        date_time = datetime.now().strftime("%d-%m-%YT%H_%M_%S")
        user=data["username"]
        db_name=data["database"]
        url=data["server"]
        passwd=data["password"]
        file_name = "{}-{}.sql".format(db_name, date_time)
        cmd ="docker exec --user postgres c7-odoo pg_dump {}>{}".format(db_name, file_name)
        cls.ssh_exec_cmd(url, user, passwd, cmd)
        ls=cls.show_files(url, user, passwd)
        status = "failed"
        if (ls.find(file_name)>-1): status= "success"

        return {
            "status": status,
            "file_name": file_name,
            "screenshot": ls
        }

    @classmethod
    def show_files(cls, url: str, user: str, passwd: str) ->str:
        return cls.ssh_exec_cmd(url, user, passwd, "ls")

    @classmethod
    def git_pull(cls, data) ->str:
        user=data["username"]
        url=data["server"]
        passwd=data["password"]
        return cls.ssh_exec_cmd(url, user, passwd, "git -C {} pull origin".format(data["folder"]))

    @classmethod
    def git_checkout(cls, data, branch) ->str:
        user=data["username"]
        url=data["server"]
        passwd=data["password"]
        folder = data["folder"]
        command = "git -C {0} checkout {1}". format(folder, branch)
        print(command)
        return cls.ssh_exec_cmd(url, user, passwd, command)


    

    @classmethod
    def git_branch(cls, data) ->str:
        user=data["username"]
        url=data["server"]
        passwd=data["password"]
        return cls.ssh_exec_cmd(url, user, passwd, "git -C {} branch -a".format(data["folder"]))

    @classmethod
    def import_backup(cls, data_old_server: dict, data_new_server: dict):
        cls.create_backup(data_old_server.url, data_old_server.user, data_old_server.passwd, data_old_server.db_name)
        # TODO: create method to copy backup
        #cls.copy_backup(data_old_server, data_new_server)
        # TODO: create method create db and upload database
        # TODO: update database in odoo


        
        
        
#cls.create_backup("bbbox.c7system.com", "root", "7Spiritual71**", "bbbox")
#Backuper.ssh_exec_cmd("bbbox.c7system.com", "root", "7Spiritual71**", 'docker ps', 22)
#scp root@bbbox.c7system.com:~/bbbox.sql  bbbox.sql
#Backuper.create_backup("bbbox.c7system.com", "root", "7Spiritual71**", 'docker exec --user postgres c7-odoo pg_dump bbbox>bbbox.sql', 22)

#Backuper.create_backup("bbbox.c7system.com", "root", "7Spiritual71**", "bbbox")

#Backuper.create_backup("concesiones.c7system.com", "root", "7Spiritual71**", "concesiones")
